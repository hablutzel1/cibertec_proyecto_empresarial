-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.6.24 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla virtualexam.alternative: ~26 rows (aproximadamente)
/*!40000 ALTER TABLE `alternative`
  DISABLE KEYS */;
INSERT INTO `alternative` (`id`, `correct`, `text`, `question_id`) VALUES
  (1, b'1', 'Blanco', 1),
  (2, b'0', 'Negro', 1),
  (3, b'0', 'Marrón', 1),
  (4, b'0', 'Perú', 2),
  (5, b'1', 'Brasil', 2),
  (6, b'0', 'Chile', 2),
  (7, b'1', 'PHP', 3),
  (8, b'0', 'C++', 3),
  (9, b'1', 'Hibernate', 4),
  (10, b'0', 'JDBC', 4),
  (11, b'0', 'MyBatis', 4),
  (12, b'1', 'Spring JDBC', 5),
  (13, b'0', 'Hibernate', 5),
  (14, b'0', 'OpenJPA', 5),
  (15, b'1', 'Huacho', 6),
  (16, b'1', 'Cañete', 6),
  (17, b'0', 'Suiza', 7),
  (18, b'0', 'España', 7),
  (19, b'1', 'Perú', 7),
  (20, b'1', 'Chile', 7);
/*!40000 ALTER TABLE `alternative`
  ENABLE KEYS */;

-- Volcando datos para la tabla virtualexam.exam: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `exam`
  DISABLE KEYS */;
INSERT INTO `exam` (`id`, `name`) VALUES
  (1, 'Examen de cultura general'),
  (2, 'Examen de programación'),
  (3, 'Examen de geografía');
/*!40000 ALTER TABLE `exam`
  ENABLE KEYS */;

-- Volcando datos para la tabla virtualexam.question: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `question`
  DISABLE KEYS */;
INSERT INTO `question` (`id`, `multipleSelection`, `text`, `exam_id`) VALUES
  (1, b'0', '¿De qué color era el caballo blanco de San Martín?', 1),
  (2, b'0', '¿Cuál es el país más grande de América Latina?', 1),
  (3, b'0', 'Es un lenguaje interpretado', 2),
  (4, b'0', 'Es un framework/librería de tipo ORM', 2),
  (5, b'0', 'No es una implementación de JPA', 2),
  (6, b'1', 'Pertenecen a Lima', 3),
  (7, b'1', 'No son paises de Europa', 3);
/*!40000 ALTER TABLE `question`
  ENABLE KEYS */;

-- Volcando datos para la tabla virtualexam.resolvedexam: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `resolvedexam`
  DISABLE KEYS */;
INSERT INTO `resolvedexam` (`exam_id`, `student_username`) VALUES
  (2, 'e_1001');
/*!40000 ALTER TABLE `resolvedexam`
  ENABLE KEYS */;

-- Volcando datos para la tabla virtualexam.selectedalternative: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `selectedalternative`
  DISABLE KEYS */;
INSERT INTO `selectedalternative` (`resolvedExam_exam_id`, `resolvedExam_student_username`, `alternative_id`) VALUES
  (2, 'e_1001', 7),
  (2, 'e_1001', 10),
  (2, 'e_1001', 12);
/*!40000 ALTER TABLE `selectedalternative`
  ENABLE KEYS */;

-- Volcando datos para la tabla virtualexam.student: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `student`
  DISABLE KEYS */;
INSERT INTO `student` (`username`, `fullName`, `password`) VALUES
  ('e_1001', 'Juan Carlos Pérez Vargas', 'e_1001');
/*!40000 ALTER TABLE `student`
  ENABLE KEYS */;

/*!40101 SET SQL_MODE = IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS = IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
