package pe.edu.cibertec.virtualexam.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@WebFilter(filterName = "SecurityFilter", urlPatterns = "/*")
public class SecurityFilter implements Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        // TODO research what javax.servlet.http.HttpServletRequest.getRequestURI() does example.
        String requestURI = request.getRequestURI();
        if (requestURI.startsWith("/javax.faces.resource/") || requestURI.equals("/login.xhtml")) { // Publicly available resources.
            chain.doFilter(servletRequest, servletResponse);
        } else if (session.getAttribute("username") == null) { // User not logged in.
            if (isAjax(request)) {
                // Triggering a JSF redirect.
                response.getWriter().print(xmlPartialRedirectToPage(request, "/login.xhtml"));
                response.flushBuffer();
            } else {
                response.sendRedirect("/login.xhtml");
            }

        } else { // User logged in, do centralized/programmatic authorization check.
            String currentProfile = (String) session.getAttribute("profile");
            if (currentProfile.equals("administrator")) {
                Set<String> authorizedPages = new HashSet<>();
                authorizedPages.add("/");
                authorizedPages.add("/index.xhtml");
                authorizedPages.add("/exams.xhtml");
                authorizedPages.add("/examDetails.xhtml");
                if (!authorizedPages.contains(requestURI)) {
                    throw new SecurityException();
                }
            } else if (currentProfile.equals("student")) {
                if (requestURI.equals("/")){ // An special case to manage the student trying to get to the root of the context path.
                    response.sendRedirect("/studentHome.xhtml");
                } else {
                    Set<String> authorizedPages = new HashSet<>();
                    authorizedPages.add("/studentHome.xhtml");
                    authorizedPages.add("/takeExam.xhtml");
                    if (!authorizedPages.contains(requestURI)) {
                        throw new SecurityException();
                    }
                }
            } else { // Another unknown profile.
                throw new SecurityException();
            }
            chain.doFilter(servletRequest, servletResponse);
        }
    }

    public void init(FilterConfig config) throws ServletException {
    }

    // See http://forum.primefaces.org/viewtopic.php?f=3&t=16735#p50842.
    private String xmlPartialRedirectToPage(HttpServletRequest request, String page) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version='1.0' encoding='UTF-8'?>");
        sb.append("<partial-response><redirect url=\"").append(request.getContextPath()).append(request.getServletPath()).append(page).append("\"/></partial-response>");
        return sb.toString();
    }

    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

}
