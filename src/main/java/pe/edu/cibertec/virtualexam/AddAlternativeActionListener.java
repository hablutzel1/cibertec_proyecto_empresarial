package pe.edu.cibertec.virtualexam;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.panelgrid.PanelGrid;
import pe.edu.cibertec.virtualexam.util.Utils;

import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import java.util.List;

import static pe.edu.cibertec.virtualexam.util.Utils.findComponent;

// TODO check if it is possible to maintain this Java object alive during all user interaction with a view, maybe by using something like @ViewScoped. Note that it is currently being recreated irrespective that during the call to 'javax.faces.component.UICommand.addActionListener()' only one instance was created.
public class AddAlternativeActionListener implements ActionListener {

    @Override
    public void processAction(ActionEvent event) throws AbortProcessingException {
        Panel questionsPanel = (Panel) ((CommandButton) event.getSource()).getParent();
        List<UIComponent> questionsPanelChildren = questionsPanel.getChildren();

        // FIXME stop relying that the fifth component will always be the HtmlInputHidden, iterate all children and search for it.
        HtmlInputHidden questionIdHiddenInput = (HtmlInputHidden) questionsPanelChildren.get(5);
        String questionId = (String) (questionIdHiddenInput).getValue();

        PanelGrid alternativePanelGrid = new PanelGrid();
        List<UIComponent> alternativePanelChildren = alternativePanelGrid.getChildren();
        alternativePanelGrid.setColumns(3);
        OutputLabel outputLabel = new OutputLabel();
        outputLabel.setValue("Alternativa");
        alternativePanelChildren.add(outputLabel);

        //region Calculating next alternative id.
        int nextAlternativeIndex = 0;
        String nextAlternativeId;
        while (true) {
            nextAlternativeId = Utils.constructAlternativeId(questionId, ++nextAlternativeIndex);
            UIComponent component = findComponent(nextAlternativeId);
            if (component == null) {
                break;
            }
        }
        //endregion

        InputText alternativeInputText = new InputText();
        alternativeInputText.setId(nextAlternativeId);
        alternativePanelChildren.add(alternativeInputText);

        Application application = FacesContext.getCurrentInstance().getApplication();
        HtmlSelectBooleanCheckbox alternativeCheckbox = (HtmlSelectBooleanCheckbox)application.createComponent(HtmlSelectBooleanCheckbox.COMPONENT_TYPE);
        alternativeCheckbox.setId(Utils.constructCorrectAlternativeCheckboxName(nextAlternativeId));
        alternativePanelChildren.add(alternativeCheckbox);
        questionsPanelChildren.add(alternativePanelGrid);
    }

}
