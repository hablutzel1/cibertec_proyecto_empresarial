package pe.edu.cibertec.virtualexam.util;

import com.sun.faces.component.visit.FullVisitContext;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.component.UIViewRoot;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import javax.faces.context.FacesContext;

public class Utils {

    // TODO check if this call couldn't be replaced with a call to FacesContext.getCurrentInstance().getViewRoot().findComponent(id).
    public static UIComponent findComponent(final String id) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        final UIComponent[] found = new UIComponent[1];
        root.visitTree(new FullVisitContext(context), new VisitCallback() {
            @Override
            public VisitResult visit(VisitContext context, UIComponent component) {
                if (component.getId().equals(id)) {
                    found[0] = component;
                    return VisitResult.COMPLETE;
                }
                return VisitResult.ACCEPT;
            }
        });

        return found[0];

    }

    public static String constructAlternativeId(String questionId, int nextAlternativeIndex) {
        return questionId + "_alternative_" + nextAlternativeIndex;
    }

    public static String constructCorrectAlternativeCheckboxName(String questionId) {
        return questionId + "_correct_alternative";
    }

    public static String constructMultipleSelectionId(String questionId) {
        return questionId + "_is_multiple_selection";
    }

    public static UIOutput createHtmlComponent(String html) {
        UIOutput verbatim = new UIOutput();
        verbatim.setRendererType("javax.faces.Text");
        verbatim.getAttributes().put("escape", false);
        verbatim.setValue(html);
        return verbatim;
    }

}
