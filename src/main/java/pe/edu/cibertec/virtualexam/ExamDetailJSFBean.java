package pe.edu.cibertec.virtualexam;

import pe.edu.cibertec.virtualexam.model.Exam;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
public class ExamDetailJSFBean {

    @ManagedProperty(value = "#{param.id}")
    private Integer examId;

    private Exam exam;

    public void setExamId(Integer examId) {
        this.examId = examId;
    }

    @PostConstruct
    public void init() {
        if (examId != null) {
            exam = Exam.getExam(this.examId);
        }
    }

    public Exam getExam() {
        return exam;
    }

}
