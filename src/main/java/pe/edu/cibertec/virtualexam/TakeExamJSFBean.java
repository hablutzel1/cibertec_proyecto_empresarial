package pe.edu.cibertec.virtualexam;

import pe.edu.cibertec.virtualexam.model.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@ManagedBean
@ViewScoped
public class TakeExamJSFBean {

    private Exam exam;

    // TODO instead of sending the Question object in a Map, try to just append the choosen alternative objects (or at least its ids) to a List or array from the JSF page.
    private Map<Question, Object> questionsAndChoosenAlternativeId = new LinkedHashMap<>();

    public Map<Question, Object> getQuestionsAndChoosenAlternativeId() {
        return questionsAndChoosenAlternativeId;
    }

    @PostConstruct
    public void init() {
        Map<String, String> requestParameterMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        exam = Exam.getExam(Integer.valueOf(requestParameterMap.get("id")));
        Set<Question> questions = exam.getQuestions();
        for (Question question : questions) {
            questionsAndChoosenAlternativeId.put(question, null);
        }
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public void submitAnswers(ActionEvent event) throws IOException {
        for (Object questionSelectedAlternativesIds : questionsAndChoosenAlternativeId.values()) {
            if (selectedIdsToArray(questionSelectedAlternativesIds).length == 0) { // If any question doesn't have at least one selected alternative.
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_WARN, "Se requiere que todas las preguntas tengan por lo menos una alternativa seleccionada.", null));
                return;
            }
        }

        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        Student currentStudent = (Student) sessionMap.get("student");
        // TODO check: is the following code as clear as possible?.
        ResolvedExam resolvedExam = new ResolvedExam();
        resolvedExam.setStudent(currentStudent);
        resolvedExam.setExam(exam);
        Set<SelectedAlternative> selectedAlternatives = new HashSet<>();
        for (Object questionSelectedAlternativesIds : questionsAndChoosenAlternativeId.values()) {
            String[] currentQuestionSelectedAlternativeIds = selectedIdsToArray(questionSelectedAlternativesIds);
            for (String selectedAlternativeId : currentQuestionSelectedAlternativeIds) {
                SelectedAlternative choosenAlternative = new SelectedAlternative();
                Alternative alternative = new Alternative();
                alternative.setId(Integer.valueOf(selectedAlternativeId));
                choosenAlternative.setAlternative(alternative);
                choosenAlternative.setResolvedExam(resolvedExam);
                selectedAlternatives.add(choosenAlternative);
            }
        }
        resolvedExam.setSelectedAlternatives(selectedAlternatives);
        resolvedExam.save();
        // TODO maybe display a success message.
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.redirect("studentHome.xhtml");
    }

    private String[] selectedIdsToArray(Object selectedAlternativeIds) {
        if (selectedAlternativeIds instanceof String[]) { // expected for multiple selection.
            return (String[])selectedAlternativeIds;
        } else {
            if (selectedAlternativeIds == null) {
                return new String[0];
            } else {
                return new String[]{(String) selectedAlternativeIds};
            }
        }
    }
}
