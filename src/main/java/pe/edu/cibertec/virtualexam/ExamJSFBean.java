package pe.edu.cibertec.virtualexam;

import org.apache.commons.lang.StringUtils;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panel.Panel;
import org.primefaces.component.panelgrid.PanelGrid;
import pe.edu.cibertec.virtualexam.model.Alternative;
import pe.edu.cibertec.virtualexam.model.Exam;
import pe.edu.cibertec.virtualexam.model.Question;
import pe.edu.cibertec.virtualexam.util.Utils;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputHidden;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@ManagedBean
// TODO research on '@ViewScoped' and how does it maintain a Java object alive across requests.
@ViewScoped
// TODO determine what is the recommended naming style for a @ManagedBean class.
public class ExamJSFBean {

    private Exam exam;

    // NOTE that this variable state (actually its enclosing object itself) is being preserved between requests :).
    private int questionsCount;

    public ExamJSFBean() {
        // TODO check if it is possible that it gets created automatically.
        exam = new Exam();
    }

    public void addQuestion(ActionEvent event) {
        UIComponent questionsPanel = Utils.findComponent("questionsPanel");
        if (questionsPanel != null) {
            Panel questionPanel = new Panel();
            List<UIComponent> questionPanelChildren = questionPanel.getChildren();
            PanelGrid panelGrid = new PanelGrid();
            List<UIComponent> panelGridChildren = panelGrid.getChildren();
            panelGrid.setColumns(2);
            OutputLabel outputLabel = new OutputLabel();
            outputLabel.setValue("Pregunta");
            panelGridChildren.add(outputLabel);
            InputText questionInputText = new InputText();
            String questionId = "question_" + ++questionsCount;
            questionInputText.setId(questionId);
            panelGridChildren.add(questionInputText);
            questionPanelChildren.add(panelGrid);
            // TODO try avoid HTML line breaks everywhere.
            questionPanelChildren.add(Utils.createHtmlComponent("<br/>"));
            CommandButton addAlternativeCommandButton = new CommandButton();
            // TODO research on org.primefaces.component.commandbutton.CommandButton.setUpdate().
            addAlternativeCommandButton.setUpdate("questionsPanel");
            addAlternativeCommandButton.setValue("Añadir alternativa");
            addAlternativeCommandButton.addActionListener(new AddAlternativeActionListener());
            questionPanelChildren.add(addAlternativeCommandButton);
            questionPanelChildren.add(Utils.createHtmlComponent("<br/>"));
            questionPanelChildren.add(Utils.createHtmlComponent("<br/>"));
            HtmlInputHidden questionIdHiddenInput = new HtmlInputHidden();
            questionIdHiddenInput.setValue(questionId);
            questionPanelChildren.add(questionIdHiddenInput);
            PanelGrid multipleSelectionPanel = new PanelGrid();
            List<UIComponent> multipleSelectionPanelChildren = multipleSelectionPanel.getChildren();
            OutputLabel multipleSelectionLabel = new OutputLabel();
            multipleSelectionLabel.setValue("Selección múltiple");
            multipleSelectionPanelChildren.add(multipleSelectionLabel);
            Application application = FacesContext.getCurrentInstance().getApplication();
            HtmlSelectBooleanCheckbox multipleSelectionCheckbox = (HtmlSelectBooleanCheckbox)application.createComponent(HtmlSelectBooleanCheckbox.COMPONENT_TYPE);
            multipleSelectionCheckbox.setId(Utils.constructMultipleSelectionId(questionId));
            multipleSelectionPanelChildren.add(multipleSelectionCheckbox);
            questionPanelChildren.add(multipleSelectionPanel);
            questionPanelChildren.add(Utils.createHtmlComponent("<br/>"));
            questionsPanel.getChildren().add(questionPanel);
        }
    }

    public void save(ActionEvent event) throws IOException, HeuristicRollbackException, RollbackException, HeuristicMixedException, SystemException {

        //region Binding questions and alternatives manually.
        // TODO try to bind questions and alternatives automatically to JSF components, see pe.edu.cibertec.virtualexam.ExamJSFBean#exam for the candidate parent object.
        Set<Question> questions = new LinkedHashSet<>();
        for (int i = 1; i <= questionsCount; i++) {
            String questionId = "question_" + i;
            InputText questionInputText = (InputText) Utils.findComponent(questionId);
            Question question = new Question();
            question.setExam(exam);
            question.setText((String) questionInputText.getValue());
            List<Alternative> alternatives = new ArrayList<Alternative>();
            question.setMultipleSelection(((HtmlSelectBooleanCheckbox) Utils.findComponent(Utils.constructMultipleSelectionId(questionId))).isSelected());
            int idx = 0;
            boolean atLeastOneCorrectAlternativeAlreadySelected = false;
            while (true) {
                // TODO try to reuse this block, see pe.edu.cibertec.virtualexam.AddAlternativeActionListener.processAction().
                String alternativeId = Utils.constructAlternativeId(questionId, ++idx);
                InputText alternativeInputText = (InputText) Utils.findComponent(alternativeId);
                if (alternativeInputText == null) {
                    break;
                }
                String alternativeText = (String) alternativeInputText.getValue();
                Alternative alternative = new Alternative();
                alternative.setQuestion(question);
                alternative.setText(alternativeText);
                HtmlSelectBooleanCheckbox alternativeCheckbox = (HtmlSelectBooleanCheckbox) Utils.findComponent(Utils.constructCorrectAlternativeCheckboxName(alternativeId));
                boolean alternativeSelected = alternativeCheckbox.isSelected();
                if (!question.isMultipleSelection() && atLeastOneCorrectAlternativeAlreadySelected && alternativeSelected){
                    addValidationError("Para las preguntas de selección simple se debe marcar una sola alternativa.");
                    return;
                }
                alternative.setCorrect(alternativeSelected);
                atLeastOneCorrectAlternativeAlreadySelected |= alternativeSelected;
                alternatives.add(alternative);
            }
            if (!atLeastOneCorrectAlternativeAlreadySelected){
                addValidationError("Se debe seleccionar al menos una alternativa correcta para cada pregunta.");
                return;
            }
            question.setAlternatives(alternatives);
            questions.add(question);
        }

        exam.setQuestions(questions);
        //endregion

        //region Validation.
        // TODO research on JSF recommended validation facilities.
        // TODO check the validation mechanism so several validation problems could be reported to the user instead of only the first problem found with an immediate return.
        if (StringUtils.isBlank(exam.getName())) {
            addValidationError("El nombre del examen es requerido.");
            return;
        }

        if (exam.getQuestions().size() == 0) {
            addValidationError("Se requiere registrar al menos una pregunta.");
            return;
        }
        for (Question question : questions) {
            if (StringUtils.isBlank(question.getText())) {
                addValidationError("El texto para cada pregunta es requerido.");
                return;
            }
            if (!(question.getAlternatives().size() > 1)) {
                addValidationError("Se requiere que todas las preguntas tengan más de una alternativa.");
                return;
            }
            for (Alternative alternative : question.getAlternatives()) {
                if (StringUtils.isBlank(alternative.getText())) {
                    addValidationError("El texto para cada alternativa es requerido.");
                    return;
                }
            }
        }
        //endregion

        exam.save();
        triggerPageReload();
    }


    private void addValidationError(String summary) {
        FacesContext.getCurrentInstance().addMessage(null,
                new FacesMessage(FacesMessage.SEVERITY_WARN, summary, null));
    }

    // TODO check if this is the recommended JSF way to clean up all the view state.
    private void triggerPageReload() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    // TODO check if it would be OK (and possible) in design terms to call pe.edu.cibertec.virtualexam.model.Exam.getAllExams straight from the JSF view.
    public List<Exam> getAllExams() {
        return Exam.getAllExams();
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }
}