package pe.edu.cibertec.virtualexam;

import pe.edu.cibertec.virtualexam.model.Student;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.Map;

@ManagedBean
// TODO check: what about @javax.faces.bean.SessionScoped for this bean?. See http://www.javaknowledge.info/primefaces-login-example-using-mysql-db/ instead of managing the interaction with the session by hand.
public class LoginJSFBean {

    public static final String USERNAME_SESSION_KEY = "username";
    public static final String PROFILE_SESSION_KEY = "profile";
    public static final String STUDENT_SESSION_KEY = "student";

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void doLogin() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        // TODO make the admin user(s) to come from database.
        if (username.equals("admin") && password.equals("admin")) {
            sessionMap.put(USERNAME_SESSION_KEY, username);
            sessionMap.put(PROFILE_SESSION_KEY, "administrator");
            externalContext.redirect("index.xhtml");
        } else {
            Student student = Student.findByUsernameAndPassword(username, password);
            if (student != null) {
                sessionMap.put(USERNAME_SESSION_KEY, username);
                sessionMap.put(PROFILE_SESSION_KEY, "student");
                sessionMap.put(STUDENT_SESSION_KEY, student);
                externalContext.redirect("studentHome.xhtml");
            } else {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage("Usuario y/o contraseña inválidos."));
            }
        }
    }

    public void doLogout() throws IOException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.getSessionMap().remove(USERNAME_SESSION_KEY);
        externalContext.getSessionMap().remove(PROFILE_SESSION_KEY);
        externalContext.getSessionMap().remove(STUDENT_SESSION_KEY);
        externalContext.redirect("login.xhtml");
    }
}
