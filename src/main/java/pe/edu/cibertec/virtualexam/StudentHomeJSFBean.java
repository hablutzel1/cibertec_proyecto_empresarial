package pe.edu.cibertec.virtualexam;

import pe.edu.cibertec.virtualexam.model.Exam;
import pe.edu.cibertec.virtualexam.model.ResolvedExam;
import pe.edu.cibertec.virtualexam.model.Student;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ManagedBean
public class StudentHomeJSFBean {

    public Set<ResolvedExam> getResolvedExams() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        Student currentStudent = (Student) sessionMap.get("student");
        return currentStudent.getResolvedExams();
    }

    public List<Exam> getPendingExams() {
        List<Exam> pendingExams = new ArrayList<>();
        for (Exam exam : Exam.getAllExams()) {
            boolean alreadyResolved = false;
            for (ResolvedExam resolvedExam : getResolvedExams()) {
                if (resolvedExam.getExam().equals(exam)) {
                    alreadyResolved = true;
                }
            }
            if (!alreadyResolved) {
                pendingExams.add(exam);
            }
        }
        return pendingExams;
    }

}