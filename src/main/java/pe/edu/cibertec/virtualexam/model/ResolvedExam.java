package pe.edu.cibertec.virtualexam.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class ResolvedExam implements Serializable {

    @EmbeddedId
    private ResolvedExamPK id;
    @MapsId("student")
    @ManyToOne
    private Student student;
    @MapsId("exam")
    @ManyToOne
    private Exam exam;
    @Transient
    private double grade;
    @OneToMany(mappedBy = "resolvedExam")
    private Set<SelectedAlternative> selectedAlternatives;

    public ResolvedExamPK getId() {
        return id;
    }

    public void setId(ResolvedExamPK id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Set<SelectedAlternative> getSelectedAlternatives() {
        return selectedAlternatives;
    }

    public void setSelectedAlternatives(Set<SelectedAlternative> selectedAlternatives) {
        this.selectedAlternatives = selectedAlternatives;
    }

    public void save() {
        // If the composite PK class instance is not created happens: org.hibernate.id.IdentifierGenerationException: null id generated for:class pe.edu.cibertec.virtualexam.model.ResolvedExam.
        ResolvedExamPK resolvedExamPK = new ResolvedExamPK();
        resolvedExamPK.setExam(exam.getId());
        resolvedExamPK.setStudent(student.getUsername());
        setId(resolvedExamPK);
        // Cascade operation to save SelectedAlternatives is failing, so we make one step at the time. FIXME don't do this :S, look for the right way.
        // First we save the SelectedAlternative list.
        Set<SelectedAlternative> savedSelectedAlternativeList = getSelectedAlternatives();
        // Unset it from the ResolvedExam entity.
        setSelectedAlternatives(null);

        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        em.getTransaction().begin();
        // And persist ResolvedExam only.
        em.merge(this);

        // Then we persist each SelectedAlternative :S.
        for (SelectedAlternative selectedAlternative : savedSelectedAlternativeList) {
            selectedAlternative.setResolvedExam(this);
            em.persist(selectedAlternative);
        }
        em.getTransaction().commit();
        em.close();
    }


    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }


}

