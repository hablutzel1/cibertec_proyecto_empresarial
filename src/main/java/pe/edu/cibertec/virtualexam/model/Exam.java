package pe.edu.cibertec.virtualexam.model;

import javax.persistence.*;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.SystemException;
import java.util.List;
import java.util.Set;

@Entity
public class Exam {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    // TODO check: maybe it is unnecessary usage ofjavax.persistence.FetchType.EAGER.
    // TODO research on javax.persistence.CascadeType.PERSIST and check if if it is the recommended way to save entire hierarchies.
    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "exam", fetch = FetchType.EAGER)
    private Set<Question> questions;

    public static Exam getExam(Integer id) {
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        Query query = em.createQuery("select e from Exam e where e.id = ?1");
        query.setParameter(1, id);
        Exam singleResult = (Exam) query
                .getSingleResult();
        // TODO check maybe javax.persistence.EntityManager.close should always be called in a finally block?.
        em.close();
        return singleResult;
    }

    public static List<Exam> getAllExams() {
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        Query query = em.createQuery("select e from Exam e");
        List resultList = query
                .getResultList();
        em.close();
        return resultList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void save() throws HeuristicRollbackException, javax.transaction.RollbackException, HeuristicMixedException, SystemException {
        // TODO check: too much boilerplate code, try to simplify delegating tx management to framework/container.
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        em.getTransaction().begin();
        em.persist(this);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exam exam = (Exam) o;

        return id != null ? id.equals(exam.id) : exam.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
