package pe.edu.cibertec.virtualexam.model;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.Query;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Student {

    @Id
    private String username;
    private String password;
    private String fullName;

    public static Student findByUsernameAndPassword(String username, String password) {
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        // FIXME do not store the plain text password in the DB.
        Query query = em.createQuery("select e from Student e where e.username = ?1 and e.password = ?2");
        query.setParameter(1, username);
        query.setParameter(2, password);
        List list = query
                .getResultList();
        em.close();
        if (list.size() > 0) {
            return (Student) list.get(0);
        } else {
            return null;
        }
    }

    static List<Student> getAll() {
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        Query query = em.createQuery("select e from Student e");
        List resultList = query
                .getResultList();
        em.close();
        return resultList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Set<ResolvedExam> getResolvedExams() {
        EntityManager em = EMFSingletonFactory.getEMF().createEntityManager();
        Query query = em.createQuery("select e from ResolvedExam e where e.student = ?1");
        query.setParameter(1, this);
        List<ResolvedExam> resolvedExamList = query
                .getResultList();
        for (ResolvedExam resolvedExam : resolvedExamList) {
            Set<Question> allExamQuestions = resolvedExam.getExam().getQuestions();
            double questionValue = 20.0 / allExamQuestions.size();
            double total = 0;
            for (Question currentQuestion : allExamQuestions) {
                List<Alternative> currentQuestionAlternatives = currentQuestion.getAlternatives();
                Set<Alternative> correctAlternativesForThisQuestion = new LinkedHashSet<>();
                for (Alternative alternative : currentQuestionAlternatives) {
                    if (alternative.isCorrect()){
                        correctAlternativesForThisQuestion.add(alternative);
                    }
                }
                Set<Alternative> chosenAlternativesForThisQuestion = new LinkedHashSet<>();
                for (SelectedAlternative selectedAlternative : resolvedExam.getSelectedAlternatives()) {
                    if (selectedAlternative.getAlternative().getQuestion().equals(currentQuestion)){ // Filtering the selected alternatives for this question only.
                        chosenAlternativesForThisQuestion.add(selectedAlternative.getAlternative());
                    }
                }
                if (chosenAlternativesForThisQuestion.equals(correctAlternativesForThisQuestion)){ // Sets with same size and items.
                    total += questionValue;
                }
            }
            // TODO evaluate to persist the rounded value instead of calculating it, so it could be simply queried later.
            total = Math.round(total);
            resolvedExam.setGrade(total);
        }
        // TODO check maybe javax.persistence.EntityManager.close should always be called in a finally block?.
        em.close();
        return new LinkedHashSet<>(resolvedExamList);
    }
}
