package pe.edu.cibertec.virtualexam.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class SelectedAlternative implements Serializable {

    @Id
    @ManyToOne
    private ResolvedExam resolvedExam;
    @Id
    @ManyToOne
    private Alternative alternative;

    public ResolvedExam getResolvedExam() {
        return resolvedExam;
    }

    public void setResolvedExam(ResolvedExam resolvedExam) {
        this.resolvedExam = resolvedExam;
    }

    public Alternative getAlternative() {
        return alternative;
    }

    public void setAlternative(Alternative alternative) {
        this.alternative = alternative;
    }

}
