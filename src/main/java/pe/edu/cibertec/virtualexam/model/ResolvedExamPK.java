package pe.edu.cibertec.virtualexam.model;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ResolvedExamPK implements Serializable {

    private String student;
    private Integer exam;

    // TODO check: no need for them to be public?. It looks like there isn't.
    String getStudent() {
        return student;
    }

    void setStudent(String student) {
        this.student = student;
    }

    Integer getExam() {
        return exam;
    }

    void setExam(Integer exam) {
        this.exam = exam;
    }

}
