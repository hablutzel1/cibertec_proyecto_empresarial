package pe.edu.cibertec.virtualexam.model;

import org.hibernate.internal.SessionImpl;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

@WebListener()
public class EMFSingletonFactory implements ServletContextListener {

    // TODO try to use annotations instead to inject the EntityManagerFactory/EntityManager in the classes requiring persistence. Maybe it would require EJBs or Spring or something alike.
    private static EntityManagerFactory emf;

    static EntityManagerFactory getEMF() {
        // Innocent singleton mechanism.
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory("virtual-exam");
            if (Student.getAll().size() == 0) { // If no Students we assume that our sample data hasn't been loaded already.
                EntityManager em = emf.createEntityManager();
                em.getTransaction().begin();
                SessionImpl session = (SessionImpl) em.getDelegate();
                try {
                    importSQL(session.connection(), EMFSingletonFactory.class.getResourceAsStream("/sample.sql"));
                } catch (SQLException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
                em.getTransaction().commit();
                em.close();
            }
        }
        return emf;
    }

    /**
     * See http://stackoverflow.com/a/1498029/320594.
     */
    private static void importSQL(Connection conn, InputStream in) throws SQLException {
        Scanner s = new Scanner(in);
        s.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement st = null;
        try {
            st = conn.createStatement();
            while (s.hasNext()) {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    st.execute(line);
                }
            }
        } finally {
            if (st != null) st.close();
        }
    }

    public void contextInitialized(ServletContextEvent sce) {
        // Forcing triggering 'hibernate.hbm2ddl.auto' during startup for development speedup.
        getEMF();
    }

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Calling javax.persistence.EntityManagerFactory.close from javax.servlet.ServletContextListener.contextDestroyed.");
        if (emf != null) {
            // Otherwise Tomcat fails to stop/restart from IntelliJ IDEA.
            emf.close();
        }
    }
}
